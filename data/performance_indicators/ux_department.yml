- name: UX Hiring Actual vs Plan
  base_path: "/handbook/engineering/ux/performance-indicators/"
  parent: "/handbook/engineering/performance-indicators/#engineering-hiring-actual-vs-plan"
  definition: Employees are in the division "Engineering" and department is "UX".
  target: 67 by February 1, 2021
  org: UX Department
  health:
    level: 3
    reasons:
    - UX hiring is on plan, but we just put in place a new "two-star minimum" rule
      that might decrease offer volume.
    - 'Health: Monitor health closely'
  sisense_data:
    chart: 8528939
    dashboard: 462325
    embed: v2
- name: UX Non-Headcount Plan vs Actuals
  base_path: "/handbook/engineering/ux/performance-indicators/"
  parent: "/handbook/engineering/performance-indicators/#engineering-non-headcount-budget-vs-plan"
  definition: We need to spend our investors' money wisely. We also need to run a
    responsible business to be successful, and to one day go on the public market.
  target: Unknown until FY21 planning process
  org: UX Department
  is_key: true
  health:
    level: 3
    reasons:
    - Chart budget vs. actual over time available in periscope with clear variance
  urls:
  - https://app.periscopedata.com/app/gitlab/632490/UX-Non-Headcount-BvAs
- name: UX Average Location Factor
  base_path: "/handbook/engineering/ux/performance-indicators/"
  definition: We remain efficient financially if we are hiring globally, working asynchronously,
    and hiring great people in low-cost regions where we pay market rates. We track
    an average location factor by function and department so managers can make tradeoffs
    and hire in an expensive region when they really need specific talent unavailable
    elsewhere, and offset it with great people who happen to be in low cost areas.
  target: 
  org: UX Department
  is_key: false
  health:
    level: 2
    reasons:
    - Our average location factor trended upward to .68 due to hiring for more complex
      stage groups.
    - We are actively working with recruiting to target the right areas of the globe
      on a per role basis, and we expect to see this average go down during FY21.
  sisense_data:
    chart: 7003912
    dashboard: 462325
    embed: v2
  urls:
  - "/handbook/hiring/charts/ux-department/"
- name: UX Handbook Update Frequency
  base_path: "/handbook/engineering/ux/performance-indicators/index.html#ux-handbook-update-frequency"
  parent: "/handbook/engineering/performance-indicators/#engineering-handbook-update-frequency"
  definition: The handbook is essential to working remote successfully, to keeping
    up our transparency, and to recruiting successfully. Our processes are constantly
    evolving and we need a way to make sure the handbook is being updated at a regular
    cadence. This data is retrieved by querying the API with a python script for merge
    requests that have files matching `/source/handbook/engineering/**` or `/source/handbook/support/**`
    over time.
  target: 50
  org: UX Department
  is_key: true
  health:
    level: 0
    reasons:
    - Unknown.
  sisense_data:
    chart: 8073963
    dashboard: 621062
    shared_dashboard: a3a7d250-712d-4982-8d02-f13f7bcbbf71
    embed: v2
- name: UX Discretionary Bonus Rate
  base_path: "/handbook/engineering/ux/performance-indicators/index.html#ux-discretionary-bonuses"
  definition: Discretionary bonuses offer a highly motivating way to reward individual
    GitLab team members who really shine as they live our values. Our goal is to award
    discretionary bonuses to 10% of GitLab team members in the UX department every
    month.
  target: 10%
  org: UX Department
  is_key: false
  health:
    level: 0
    reasons:
    - We currently track bonus percentages in aggregate, but there is no easy way
      to see the percentage for each individual department.
  urls:
  - 
- name: UX Department Member MR Rate
  base_path: "/handbook/engineering/ux/performance-indicators/"
  definition: UX Department <a href="/handbook/engineering/#merge-request-rate">MR
    Rate</a> is a performance indicator showing how many changes the UX team implements
    directly in the GitLab product. We currently count all members of the UX Department
    (Directors, Managers, ICs) in the denominator, because this is a team effort.
    The <a href="https://gitlab.com/gitlab-data/analytics/blob/master/transform/snowflake-dbt/data/projects_part_of_product.csv">projects
    included</a> contributes to the overall product development efforts.
  target: TBD
  org: UX Department
  is_key: false
  health:
    level: 2
    reasons:
    - We don't yet know what a good MR rate looks like for UX. Need accurate data
      to determine.
    - UX MR rate doesn't accurately reflect all MRs to which UX contributes, because
      we often collaborate on MRs rather than opening them ouselves.
  urls:
  - https://gitlab.com/gitlab-data/analytics/-/issues/4448
  sisense_data:
    chart: 8934462
    dashboard: 686928
    shared_dashboard: 98e50197-4564-47f1-8a70-aa156c1c52e3
    embed: v2
- name: Perception of system usability
  base_path: "/handbook/engineering/ux/performance-indicators/"
  definition: The System Usability Scale (SUS) is an industry-standard survey that
    measures overall system usability based on 10 questions. Moving a SUS score upward
    even a couple of points on a large system is a significant change. The goal of
    this KPI is to understand how usability of the GitLab product rates against industry
    standards and then track trends over time. Even though UX will be responsible
    for this metric, they will need other departments such as PM and Development to
    positively affect change. See <a href="https://measuringu.com/interpret-sus-score/">Table
    1</a> for grading details.
  target: B+
  sisense_data:
    chart: 5976554
    dashboard: 462325
    embed: v2
  org: UX Department
  is_key: true
  health:
    level: 2
    reasons:
    - Perceived usability rates as a B- with long-term trend flat to slightly down.
    - Continue to action on UX Scorecards for every stage group. Focus on UX Debt.
      Work with PM to prioritize usability issues.
  urls:
  - https://docs.google.com/spreadsheets/d/1RzoYgjIH8RYiUxEJmUb7f_hTy83vTxpRtJS4AL0Ekrk/edit?usp=sharing
- name: Proactive UX Work
  base_path: "/handbook/engineering/ux/performance-indicators/"
  definition: Use customer research to validate problems and solutions to ensure we
    are building the right things in the right way. We use many research methods,
    including interviews, surveys, usability studies, findability/navigation studies,
    and analytics. Hypothesis that there is a connection between this KPI and SUS
    KPI.
  target: 2 validation issues per Product Designer per quarter
  sisense_data:
    chart: 7004937
    dashboard: 462325
    embed: v2
  org: UX Department
  is_key: true
  health:
    level: 3
    reasons:
    - In Q1 FY21, we had 30 product Designers on staff and 52 total validation issues.
      This is slightly under our target of 60.
- name: UX debt
  base_path: "/handbook/engineering/ux/performance-indicators/"
  definition: UX Debt means that for a given issue, we failed to meet defined standards
    for our Design system or for usability and feature viability standards as defined
    in agreed-upon design assets. When we fail to ship something according to defined
    standards, we track the resulting issues with a "UX debt" label. Even though UX
    will be responsible for this metric, they will need other departments such as
    PM and Development to positively affect change.
  target: Under 50 open "ux debt" issues
  sisense_data:
    chart: 6599301
    dashboard: 462325
    embed: v2
  org: UX Department
  is_key: false
  health:
    level: 2
    reasons:
    - Open "UX debt" issues are growing over time. Average days to close a "UX debt"
      issue is trending down but still unacceptable with a current average of 392
      days to close (down from 688 days in July 2019).
    - Total amount of UX debt continues to increase.
    - See the <a href="https://app.periscopedata.com/app/gitlab/641753/UX-Debt">UX
      Debt dashboard</a> for a breakdown by stage group.
    - We are actively working with PMs to prioritize UX Debt. Some stage groups are
      committing to resolving a minimum number of UX Debt issues per milestone (generally,
      a commitment of no less than one issue). We will track this effort and make
      adjustments as we see the results.
- name: Technical Writing team member MR rate
  base_path: "/handbook/engineering/ux/performance-indicators/"
  definition: This KPI tracks the number of ~documentation MRs merged every month
    across all GitLab projects which have involvement (review, collaboration, or authoring)
    from the ~"Technical Writing" team. The goal is to increase velocity over time
    as the team grows.
  target: 715 MRs per month (55 MRs per technical writer per month)
  org: UX Department
  is_key: false
  health:
    level: 3
    reasons:
    - In early May 2020, we raised the target to 55 MRs per writer per month. May
      2020 average was 47 MRs per writer. June 2020 average was 46 MRs per writer.
      July 2020 average was 56 MRs per writer.
    - We'll continue to watch this metric to determine the appropriate target.
  sisense_data:
    chart: 6223816
    dashboard: 462325
    embed: v2
- name: Distribution of Technical Writing team documentation effort
  base_path: "/handbook/engineering/ux/performance-indicators/"
  definition: Tracks the type of documentation changes involving the ~"Technical Writing"
    team, based on `docs::` scoped label applied. Labels include feature, new, improvement,
    fix, revamp, or housekeeping. Our goal is to increase the proportion of efforts
    where Technical Writers proactively improve content (improvement, new, and revamp),
    instead of just responding to a new feature or fixing a bug.
  target: 50% of MRs are focused on proactively making our docs better <a href="https://gitlab.com/gitlab-org/gitlab/-/labels?utf8=%E2%9C%93&subscribed=&search=docs%3A%3A">improvement,
    revamp, new</a>.
  org: UX Department
  is_key: false
  health:
    level: 2
    reasons:
    - 47% of Technical Writing MRs were focused on proactive improvements in April
      2020, 41% in May, 45% in June, and 52% in July.
    - We're improving consistency in MR reporting by adding scoped labels, automating
      label additions when possible, and providing reminder messages for labels that
      can't be automatically added.
  sisense_data:
    chart: 6324830
    dashboard: 462325
    embed: v2
- name: UX Department New Hire Average Location Factor
  base_path: "/handbook/engineering/ux/performance-indicators/"
  parent: "/handbook/engineering/performance-indicators/#engineering-division-new-hire-average-location-factor"
  definition: We remain efficient financially if we are hiring globally, working asynchronously,
    and hiring great people in low-cost regions where we pay market rates. We track
    an average location factor for team members hired within the past 3 months so
    hiring managers can make tradeoffs and hire in an expensive region when they really
    need specific talent unavailable elsewhere, and offset it with great people who
    happen to be in more efficient location factor areas with another hire.
  target: 0.58
  org: UX Department
  is_key: false
  health:
    level: 3
    reasons:
    - We are under our target
  sisense_data:
    chart: 9389215
    dashboard: 719538
    embed: v2
