module EditLinkHelper
  # Returns `sites/<site>` if page lives in a monorepo site, empty string if not
  def monorepo_site_path_fragment
    monorepo_site = nil

    monorepo_data = data.monorepo.to_h
    monorepo_data['blog'] = { 'paths' => ['blog'] } # Manually add in blog entry for now, it has source but isn't in monorepo.yml anymore
    monorepo_data.each do |site, entry|
      entry['paths'].each do |path|
        site_path_regex = /^#{path}/
        if current_page.file_descriptor.relative_path.to_s.match?(site_path_regex)
          monorepo_site = site
        end
      end
    end
    monorepo_site ? "sites/#{monorepo_site}/" : ''
  end
end
